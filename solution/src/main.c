#include "bmp/bmp.h"
#include "file/file.h"
#include "img/image.h"
#include <stdio.h>



int main( int argc, char** argv ) {
    (void) argc; (void) argv; // suppress 'unused parameters' warning

    if (argc < 3) {
        fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image>");
        return -1;
    }

    FILE* original_bmp = open_file(argv[1], "r");
    FILE* new_bmp = open_file(argv[2], "w");
    struct image* original_image = alloc_image(0, 0);
    from_bmp(original_bmp, original_image);
    close_file(original_bmp);
    struct image* rotated_image = rotate_image(original_image);
    to_bmp(new_bmp, rotated_image);
    close_file(new_bmp);
    printf("debug");
    free_image(original_image);
    free_image(rotated_image);
    return 0;
}
