#ifndef BMPH
#define BMPH
#include "../img/image.h"
#include <stdint.h>
#include <stdio.h>
struct bmp_header {
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
} __attribute__((packed));

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FAIL_NULL_PTR
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp( FILE* in, struct image* image );
enum write_status to_bmp( FILE* out, struct image* image );
enum read_status read_bmp_header(FILE* in, struct bmp_header* header);
enum read_status read_bmp_pixel_array(FILE* in, struct image* image, struct bmp_header* header);
void init_bmp_header(struct bmp_header* header, struct image* image);
void print_bmp_header(struct bmp_header* header);
uint8_t get_padding(size_t width);
#endif